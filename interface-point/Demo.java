public class Demo {

    public static void main(String[] args) {
        Point<Point2D> first2d = new Point2D(1.2, 2.3);
        Point<Point2D> second2d = new Point2D(3.2, 6.3);

        System.out.println(first2d.getDistance(second2d)); // ei tööta, cast parameter

        Point<Point3D> first3d = new Point3D(3.0, 1.2, 2.3);
        Point<Point3D> second3d = new Point3D(2.2, 3.2, 6.3);

        System.out.println(first3d.getDistance(second3d)); // ei tööta, cast parameter
    }
}
