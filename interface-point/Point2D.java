
public class Point2D implements Point<Point2D> { // see tundub kahtlane laiendus
    private final double x;
    private final double y;

    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public double getDistance(Point2D other) {
        return Math.sqrt(this.getDistanceSquare(other));
    }

    private double getDistanceSquare(Point2D other) {
        double dx = other.x - this.x;
        double dy = other.y - this.y;

        return dx * dx + dy * dy;
    }

}
