
public interface Point<T> {

    double getDistance(T object);

}
