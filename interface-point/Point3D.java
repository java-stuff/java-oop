
public class Point3D implements Point<Point3D> {  // see tundub kahtlane laiendus
    private final double x;
    private final double y;
    private final double z;

    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public double getDistance(Point3D other) {
        return Math.sqrt(this.getDistanceSquare(other));
    }

    private double getDistanceSquare(Point3D other) {
        double dx = other.x - this.x;
        double dy = other.y - this.y;
        double dz = other.z - this.z;

        return (dx * dx) + (dy * dy) + (dz * dz);
    }
}
