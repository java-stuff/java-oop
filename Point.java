/**
 * Ülesanne (live coding, väga üldine püstitus, aga paremini sõnastust ei mäleta)
 *           Tahame leida distantsi kahe punkt-objekti vahel.
 *           Luua punktide süsteemi jaoks vajalikud klassid ja meetodid.
 *
 * Kirjutan natuke lahti, kuidas see protsess välja nägi ja kus mul error tekkis.
 * 1. Alustasin lihtsalt sellest, et oleks olemas klass Point, kus on olemas parameetrid x ja y ning getDistance meetod
 *
 * class Point {
 *     private final double x;
 *     private final double y;
 *
 *     // konstruktor ja getDistance meetod
 * }
 *
 * 2. Seejärel tekkis küsimus, et kuidas olukorra lahendad kui tegemist ei ole kahemõõtmelise ruumiga, vaid nt kolme?
 *    Alguses polnud selge - ja ma ei osanud küsida - kas tegemist on 2 või 3 või n dimensionaalse ruumiga.
 * 
 * 3. Tekkis kerge plokk ette ja kirjutasin alloleva lahenduse, kus siis tüübi (CoordinateType) alusel otsustatakse,
 *    millise dimensionaalsusega on tegemist (2D, 3D etc).
 *
 * 4. Nüüd aga on olukord, kus Point2D.class ja Point3D.class suht samad
 *    Ehk et justkui peaks olema mingi ülemklass, kus korduvkood kokku pandud? Või tuleks mingi liidesega siduda? 
 *
 *    Kuidas oleks olnud "õigem" seda lahendada, kui on teada, et tuleb arvesse võtta mitme dimensionaalsed punktid?
 *    Oskad soovitada, millele tuleks tähelepanu pöörata? 
 *
 *    Kui küsimusi, siis kirjuta.
 */

class Point<CoordinateType> {
    public final String name;
    public final CoordinateType position;

    public Point(String name, CoordinateType position) {
        this.name = name;
        this.position = position;
    }
}

class Point3D {
    public final double x;
    public final double y;
    public final double z;

    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getDistance(Point3D other) {
        return Math.sqrt(this.getDistanceSquare(other));
    }

    private double getDistanceSquare(Point3D other) {
        double dx = other.x - this.x;
        double dy = other.y - this.y;
        double dz = other.z - this.z;

        return (dx * dx) + (dy * dy) + (dz * dz);
    }
}

class Point2D {
    public final double x;
    public final double y;

    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getDistance(Point2D other) {
        return Math.sqrt(this.getDistanceSquare(other));
    }

    private double getDistanceSquare(Point2D other) {
        double dx = other.x - this.x;
        double dy = other.y - this.y;

        return dx * dx + dy * dy;
    }
}

class Demo {

    public static void main(String[] args) {
        Point<Point2D> a = new Point<Point2D>("A", new Point2D(5.2, 8.3));
        Point<Point2D> b = new Point<Point2D>("B", new Point2D(4.3, 2.7));

        Point<Point3D> c = new Point<Point3D>("C", new Point3D(3.3, 5.3, 1.1));
        Point<Point3D> d = new Point<Point3D>("D", new Point3D(7.2, 2.6, 5.1));

        System.out.println(a.position.getDistance(b.position));
        System.out.println(c.position.getDistance(d.position));
    }
}

